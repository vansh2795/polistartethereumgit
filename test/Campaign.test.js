const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const provider = ganache.provider();
const web3 = new Web3(provider);

const compiledFactory = require('../ethereum/build/Campaignfactory.json');
const compiledCamapign = require('../ethereum/build/Campaign.json');

let accounts;
let factory;
let camapign;
let camapignAddress;

beforeEach(async() => {
  accounts = await web3.eth.getAccounts();
  factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
    .deploy({data: compiledFactory.bytecode })
    .send({from: accounts[0], gas: '1000000' });
  factory.setProvider(provider);
  await factory.methods.CreateCampaign('1000').send({
    from:  accounts[0],
    gas: '1000000'
  });

  [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
  campaign = await new web3.eth.Contract(
    JSON.parse(compiledCamapign.interface),
    campaignAddress
  );
});

describe('Campaigns', () => {
  it('deploys a factory and campaign', () => {
    assert.ok(factory.options.address);
    assert.ok(campaign.options.address);
  });
  it('marks caller',async()=>{
    const manager = await campaign.methods.manager().call();
    assert.equal(accounts[0],manager);
  });

  it('checks to see if person becomes a contributor',async()=>{
    await campaign.methods.contribute().send({
      value:'10000',
      from:accounts[1]
  });
    const isContributor = await campaign.methods.approvers(accounts[1]).call();
    assert(isContributor);
  });
  it('minimum requiremnt', async() => {
    try{
      await campaign.methods.contribute().send({
        value:'5',
        from: accounts[1]
      });
    } catch(err){
      assert(err);
    }
  })
  it('allows manager to make payment req',async() => {
    await campaign.methods.createRequest(
      'buy batteries',
      '10000',
      accounts[1])
    .send({
      from: accounts[0],
      gas:'1000000'
    });
    const request = await campaign.methods.requests(0).call();
    assert.equal('buy batteries',request.description);

  });
  it('end to end',async()=>{
    await campaign.methods.contribute().send({
      from: accounts[0],
      value: web3.utils.toWei('10','ether')
    });

    await campaign.methods.createRequest('End to end',web3.utils.toWei('5','ether'), accounts[1])
    .send({from:accounts[0], gas:'1000000'  });
    await campaign.methods.approveRequest(0).send({
      from:accounts[0],
      gas: '1000000'
    });
    await campaign.methods.finalizeRequest(0).send({
      from:accounts[0],
      gas: '1000000'
    });
    let balance = await web3.eth.getBalance(accounts[1]);
    balance = web3.utils.fromWei(balance,'ether');
    balance = parseFloat(balance);
    console.log(balance);
    assert(balance > 103);
  });
});
