import React,{Component} from 'react';
import factory from '../Ethereum/factory';
import {Card,Button} from 'semantic-ui-react';
import layout from '../components/layout'
class CampaignIndex extends Component
{
  static async getInitialProps() {
    const campaigns = await factory.methods.getDeployedCampaigns().call()
    return { campaigns };
  }

  renderCamapaigns() {
    const items = this.props.campaigns.map(address => {
      return {
        header: address,
        description: <a>View Campaigns</a>,
        fluid: true
      };
    });
    return <Card.Group items = {items} />;
  }

  render() {
    return (
    <layout>
      <div>
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/semantic-ui@2.3.0/dist/semantic.min.css">
      </link>
      <h3>Open Campaigns</h3>
      {this.renderCamapaigns()}
      <Button
        content ="Create PoliCampaign"
        icon = "add circle"
        primary />
      </div>
    </layout>
    );
  }

}
export default CampaignIndex;
