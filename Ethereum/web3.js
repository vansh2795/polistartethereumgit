import Web3 from 'web3';

let web3;

if (typeof window !== 'undefined' && window.web3 !== 'undefined') {
//In browser with metamask running
    web3 = new Web3(window.web3.currentProvider);
} else {
  // On server OR user not running metamask
  const provider = new Web3.providers.HttpProvider('https://rinkeby.infura.io/rEAg5sbyC6gfsZxDsagD');
  web3 = new Web3(provider);
}


export default web3;
