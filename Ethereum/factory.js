import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json'

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  '0xD32B68F88387e23A105EfdEd861598c37A3d7bBf'
);

export default instance;
